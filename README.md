Table of contents
-----------------

* Introduction
* Features
* Schema.org types


Introduction
------------

The **Schema.org Blueprint: Recipes Starter Kit module** provides
a starter kit that creates a Schema.org Recipe type with a Recipes view.


Features
--------

- Creates a recipes view.
- Adds a default shortcut to view Recipes (/recipes).

Schema.org types
----------------

Below are the Schema.org types created by this starter kit.

- **Recipe** (node:Recipe)  
  A recipe.  
  <https://schema.org/Recipe>

